drop database if exists aaSys;
create database if not exists aaSys;
use aaSys;


-- Tablas padres

-- Tabla de grados
 
create table grad(
aa_idol_grad int not null primary key auto_increment,
aa_nom_grad varchar(30) not null
);

-- tabla 
create table gnr(
aa_idol_gnr int not null primary key auto_increment,
aa_nom_gnr varchar(20) not null
);

-- tabla estado
create table est(
aa_idol_est int not null primary key auto_increment,
aa_nom_est varchar(50) not null
);

-- Tabla de seccion
create table scn(
aa_idol_scn int not null primary key auto_increment,
aa_nom_scn varchar(2) not null
);

-- Tabla tipo educación
create table tpd(
aa_idol_tpd int not null primary key auto_increment,
aa_nom_tpd varchar(20) not null
);


-- Tabla turno
create table trn(
aa_idol_trn int not null primary key auto_increment,
aa_nom_trn varchar(30) not null
);

-- tabla rol
create table rl(
aa_idol_rl int(11) not null primary key auto_increment,
aa_nom_rl varchar(20) not null
);

create table ms(
aa_idol_ms int(11) not null primary key auto_increment,
aa_nom_ms varchar(20) not null,
aa_cant_ms varchar(3) not null
);


-- Tablas hijas

-- Tabala tipo profesor
create table prf(
aa_idol_prf int(11) not null primary key auto_increment,
aa_nom_prf varchar(60) not null,
aa_ape_prf varchar(15) not null,
aa_dir_prf varchar(200) not null,
aa_tlf_prf varchar(15) not null,
aa_idol_grad int(11) not null,
constraint fk_grad_prf foreign key (aa_idol_grad) references grad(aa_idol_grad)

);

-- Tabla horario
create table hro(
aa_idol_hro int not null primary key auto_increment, 
aa_idol_trn int not null,
aa_hi_hro time not null,
aa_hf_hro time not null,
constraint fk_trn_hro foreign key (aa_idol_trn) references trn(aa_idol_trn)
);

-- Tabla alumno 

create table al(
aa_idol_al int not null primary key auto_increment,
aa_cod_al varchar(20) unique not null,
aa_nom_al varchar(70) not null,
aa_ape_al varchar(70) not null,
aa_fnac_al date not null,
aa_idol_gnr int not null,
aa_idol_grad int not null,
aa_idol_scn int not null,
aa_idol_tpd int not null,
aa_idol_trn int not null,
aa_idol_prf int not null,
aa_dir_al varchar(200) not null,
aa_tlf_al varchar(15) not null,
aa_pr_al varchar(70) not null,
aa_prt_al varchar(30) not null,
aa_ptlf_al varchar(15) not null,
constraint fk_gnr_al foreign key (aa_idol_gnr) references gnr(aa_idol_gnr),
constraint fk_grad_al foreign key (aa_idol_grad) references grad(aa_idol_grad),
constraint fk_scn_al foreign key (aa_idol_scn) references scn(aa_idol_scn),
constraint fk_tpd_al foreign key (aa_idol_tpd) references tpd(aa_idol_tpd),
constraint fk_trn_al foreign key (aa_idol_trn) references trn(aa_idol_trn),
constraint fk_prf_al foreign key (aa_idol_prf) references prf(aa_idol_prf)
);

-- Tabla asistencia
create table ast(
aa_idol_ast int not null primary key auto_increment,
aa_idol_est int not null,
aa_idol_al int not null,
aa_fecha_ast date not null,
constraint fk_est_ast foreign key (aa_idol_est) references est(aa_idol_est),
constraint fk_al_ast foreign key (aa_idol_al) references al(aa_idol_al)
);

-- Tabla asistencia por alumno
create table asta(
aa_idol_asta int not null primary key auto_increment,
aa_idol_al int not null,
aa_porp_asta decimal not null,
aa_porper_asta decimal not null,
aa_porf_asta decimal not null,
constraint fk_al_asta foreign key (aa_idol_al) references al(aa_idol_al)
);

-- Tabla estadisticas
create table stdc(
aa_idol_stdc int not null primary key auto_increment,
aa_idol_grad int not null,
aa_portp_stdc decimal not null,
aa_portnop_stdc decimal not null,
aa_portnap_stdc decimal not null,
aa_portper_stdc decimal not null,
aa_portnoper_stdc decimal not null,
aa_portnaper_stdc decimal not null,
aa_portf_stdc decimal not null,
aa_portnof_stdc decimal not null,
aa_portnaf_stdc decimal not null,
constraint fk_grad_stdc foreign key (aa_idol_grad) references grad(aa_idol_grad)
);


-- Tabla  usuario 
create table usr(
aa_idol_usr int not null primary key auto_increment,
aa_usr_usr varchar(20) not null,
aa_prw_user varchar(20) not null,
aa_idol_prf int not null,
aa_idol_rl int not null,
constraint fk_prf_user foreign key (aa_idol_prf) references prf(aa_idol_prf),
constraint fk_rl_user foreign key (aa_idol_rl) references rl(aa_idol_rl)
);

-- asistencia anual (para fines de historial)
create table aspa(
aa_idol_aspa int not null primary key auto_increment,
aa_idol_ast int not null,
aa_porap_aspa decimal not null,
aa_poranop_aspa decimal not null,
aa_poranap_aspa decimal not null,
aa_poraper_aspa decimal not null,
aa_poranoper_aspa decimal not null,
aa_poranaper_aspa decimal not null,
aa_poraf_aspa decimal not null,
aa_poranof_aspa decimal not null,
aa_poranaf_aspa decimal not null,
constraint fk_ast_aspa foreign key (aa_idol_ast) references ast(aa_idol_ast)
);

-- alimentacion tabla genero
insert into gnr values(1, 'masculino');
insert into gnr values(2, 'femenino');

-- alimentacion tabla grado

insert into grad values(1, 'kinder 4');
insert into grad values(2, 'Kinder 5');
insert into grad values(3, 'preparatoria');
insert into grad values(4, 'primer grado');
insert into grad values(5, 'segundo grado');
insert into grad values(6, 'tercer grado');
insert into grad values(7, 'cuarto grado');
insert into grad values(8, 'quinto grado');
insert into grad values(9, 'sexto grado');
insert into grad values(10, 'septimo grado');
insert into grad values(11, 'octavo grado');
insert into grad values(12, 'noveno grado');
insert into grad values(13, 'primero de bachillerato');
insert into grad values(14, 'segundo de bachillerato');
insert into grad values(15, 'tercero de bachillerato');

-- alimentacion tabla mes

insert into ms values(1,'enero',31);
insert into ms values(2,'febrero',29);
insert into ms values(3,'marzo',31);
insert into ms values(4,'abril',30);
insert into ms values(5,'mayo',31);
insert into ms values(6,'junio',30);
insert into ms values(7,'julio',31);
insert into ms values(8,'agosto',31);
insert into ms values(9,'septiembre',30);
insert into ms values(10,'octubre',31);
insert into ms values(11,'noviembre',30);

-- alimentacion de la tabla estado
insert into est values(1,'presente');
insert into est values(2,'ausente');
insert into est values(3,'permiso');
insert into est values(4,'vacacion');
insert into est values(5,'asueto');
insert into est values(6,'fin de semana');
insert into est values(7,'Capacitacion Maestros');
insert into est values(8,'Cuarentena');
insert into est values(9,'Emergencia Nacional');
insert into est values(10,'Catastrofe Nacional');

-- alimentacion de la tabla seccion
insert into scn values(1, 'A');

-- alimentacion de tabla tipo educacion
insert into tpd values(1,'parvularia');
insert into tpd values(2,'primaria');
insert into tpd values(3,'secundaria');
insert into tpd values(4,'bachillerato');

-- alimentacion tabla profesor
insert into prf values(1,'Alejandra','Alfaro','Colonia Maquilishuat, pasaje T, casa 89','7285-7845',1);
insert into prf values(2,'Gracia','Flores','Colonia Miravalle, block C #90','7840-6363',2);
insert into prf values(3,'Tania','Arevalo','Reparto Valle verde, senda Y, #10','6112-8817',3);
insert into prf values(4,'Lisbeth','Osuna','Reparto Valle verde, senda Z, #50','7512-9668',4);
insert into prf values(5,'Fernando','Hueso','Colonia Buena Vista, psaje G, casa 23','7145-9966',5);
insert into prf values(6,'Tomas','Marcano','Residencial Bosques del Matazano, pasaje 3, casa #12','7415-2218',6);
insert into prf values(7,'Antonio','Rammirez','Colonia Las Flores, pasaje K, casa #87','7020-9684',7);
insert into prf values(8,'Joaquin','Henriquez','Colonia la seniora del Carmen, poligono H, casa 45','7118-1281',8);
insert into prf values(9,'Irma','Palacios','Colonia Los Alpes, pasaje B-12, casa #789','7551-7862',9);
insert into prf values(10,'Karol','Baraona','Colonia Los Amates, senda U, casa #123','7133-3311',10);
insert into prf values(11,'Marisela','Carranza','Colonia Los Amates, senda R, casa #230','7001-2140',11);
insert into prf values(12,'Remberto','Barraza','Colonia las Brisas, enda B, casa 78','7699-1412',12);
insert into prf values(13,'Omar','Castellanos','Residencial Caminos del Cerro, pasaje B19, csa #2','7545-9696',13);
insert into prf values(14,'Ursula','Castillo','Colonia la Floresta, pasaje G, casa 34','7440-5512',14);

insert into rl values(1,'director');
insert into rl values(2,'profesor');
insert into rl values(3,'coordinador');

-- alimentación tabla usuario
insert into usr values(1,'PFR001','pass001',1,2);
insert into usr values(2,'PFR002','pass002',2,2);
insert into usr values(3,'PFR003','pass003',3,2);
insert into usr values(4,'PFR004','pass004',4,2);
insert into usr values(5,'PFR005','pass005',5,2);
insert into usr values(6,'PFR006','pass006',6,2);
insert into usr values(7,'PFR007','pass007',7,2);
insert into usr values(8,'PFR008','pass008',8,2);
insert into usr values(9,'PFR009','pass009',9,2);
insert into usr values(10,'PFR010','pass010',10,1);
insert into usr values(11,'PFR011','pass011',11,1);
insert into usr values(12,'PFR012','pass012',12,1);
insert into usr values(13,'PFR013','pass013',13,3);
insert into usr values(14,'PFR014','pass014',14,3);



-- alimentacion de la tabla turno
insert into trn values(1,'matutino');
insert into trn values(2,'vespertino');

-- alimentacion de la tabla horario
insert into hro values(1,1,'07:00:00','12:00:00');
insert into hro values(2,2,'13:00:00','17:30:00');

-- alimentacion alumno

insert into al values(1,'AM1-9','Armando Heriberto','Iraheta Alegria','2009-02-12',1,12,1,3,1,12,'Colonia los Alpes, casa #12','7412-7771','Maritza de Alegria','madre','7111-5244');
insert into al values(2,'AM2-9','Beatriz Maria','Melara Alfaro','2009-02-12',2,12,1,3,1,12,'Colonia Sevilla, pasaje U #12','7455-1244','Ernestina Medina','abuela','7214-8855');
insert into al values(3,'AM3-9','Carlos Alfredo','Meza Regalado','2009-02-12',1,12,1,3,1,12,'Colonia Los Amaneceres 4','7845-0000','Gonzalo Meza','padre','7411-5588');
insert into al values(4,'AM4-9','Daniela Abigail','Zetino Melendez','2009-02-12',2,12,1,3,1,12,'','7411-5544','Alfredo Meza','padre','7232-4412');
insert into al values(5,'AM5-9','Esmeralda Jeannette','Landaverde Verapaz','2009-02-12',2,12,1,3,1,12,'Colonia kasjdklsajdsaklsajskadsa ','7422-4499','Aurelio Gomez','padre','7318-2121');
insert into al values(6,'AM6-9','Fernanda Manuela','Urrutia Enamorado','2009-02-12',2,12,1,3,1,12,'','7414-9912','Pablo Urrutia','padre','7712-1221');
insert into al values(7,'AM7-9','Gema Sujey','Vadillo Vadillo','2009-02-12',2,12,1,3,1,12,'','6150-0550','Luis Vadillo','padre','6144-5121');
insert into al values(8,'AM8-9','Humberto Rafael','Marroquin Granados','2009-02-12',1,12,1,3,1,12,'Colonia los cabos ojos , pasaje 5 #23','7828-5214','Gilberto Marroquin','padre','7921-4466');
insert into al values(9,'AM9-9','Imelda Alendrina','Palomo Padilla','2009-02-12',2,12,1,3,1,12,'Colonia Santa Lucias, pje R, casa #156','7766-2211','Imelda de Padilla','madre','7314-5521');
insert into al values(10,'AM10-9','Johanna Clariza','Schmidth Cota','2009-02-12',2,12,1,3,1,12,'Residencial urbanizacion Santa Lucia, poligono R #99','7755-1245','Ana Cota','madre','7544-9661');
insert into al values(11,'AM11-9','Karen Cecilia','Cota Valencia','2009-02-12',2,12,1,3,1,12,'Residencial Venecia, pasaje U-12 #89','7845-9966','Arely Valencia','madre','6124-7899');
insert into al values(12,'AM12-9','Leucel Andrea','Vega Garcia','2009-02-12',2,12,1,3,1,12,'Colonia los Alpes, casa #67','7890-2154','Yanira Medrano','madre','7515-1514');
insert into al values(13,'AM13-9','Manuela Dolores','Almilcar Serrano','2009-02-12',2,12,1,3,1,12,'Colonia los Alpes, casa #300','7700-6600','Laura Henriquez','madre','7215-7215');
insert into al values(14,'AM14-9','Nadia Eloisa','Gutierrez Rosales','2009-02-12',2,12,1,3,1,12,'Colonia los Alpes, casa #4','7300-8911','Elsa de Rosales','madre','7451-8877');
insert into al values(15,'AM15-9','Ophelia Beatriz','Hernandez Alvarez','2009-02-12',2,12,1,3,1,12,'Colonia los Alpes, casa #56','7840-2121','Miriam Guevara','tia','7510-5564');
insert into al values(16,'AM16-9','Peter Anthon','Ramirez Mendoza','2009-02-12',1,12,1,3,1,12,'Colonia los Alpes, casa #88','7030-4020','Avelina Rmirez','abuela','7777-0200');
insert into al values(17,'AM17-9','Renato José','Garcia Jimenez','2009-02-12',1,12,1,3,1,12,'Residencial Bosques del Matazano, pasaje 10, casa #44','7477-4512','Monica Jimenez','madre','7465-8871');
insert into al values(18,'AM18-9','Silvia Andrea','Stupinian Ungo','2009-02-12',2,12,1,3,1,12,'Residencial Las Carmelitas, pasaje 3, casa #12','7966-2212','Silvia Ungo','madre','7155-9910');
insert into al values(19,'AM19-9','Tom Maximiliano','Cepeda Espino','2009-02-12',1,12,1,3,1,12,'Residencial Agua en mis ojos, pasaje 3, casa #12','6144-9645','Tomasa de Espino','madre','7855-1002');
insert into al values(20,'AM20-9','Ursula Eloisse','Gamboa Icaza','2009-02-12',2,12,1,3,1,12,'Residencial Faro de luz, pasaje Abelita, casa #12','7696-6631','Vicenta Gamboa','madre','7445-6666');
insert into al values(21,'AM21-9','Viviene Stephania','Medrano Navarro','2009-02-12',2,12,1,3,1,12,'Colonia Matazano 2, pasaje 3, casa #12','7912-2177','Ada Medrano','madre','7028-6666');
insert into al values(22,'AM22-9','William Fernandio','Fuentes Pantoja','2009-02-12',1,12,1,3,1,12,'Residencial los pinos, pasaje 3, casa #12','7212-7844','Amanda Pantoja','madre','7777-4455');
insert into al values(23,'AM23-9','Xotchitl Colette','Paniagua Gonzales','2009-02-12',2,12,1,3,1,12,'Plan del pino, ciudadela don bosco, pasaje Ramos, casa #565','7450-9678','Carolina Gonzales','madre','7745-8888');
insert into al values(24,'AM24-9','Yancy Andrea','Valdiva Zuniga','2009-02-12',2,12,1,3,1,12,'Residencial Bosques del Matazano, pasaje 3, casa #9','7710-1010','Catalina Zuniga','madre','7114-2255');


-- PA de la tabla de asistencias por alumno
DELIMITER //
CREATE PROCEDURE pa_asta(pa_aa_idol_al int)
BEGIN
set @presente = (select count(aa_idol_est) from ast  where aa_idol_est = 1 and aa_idol_al = pa_aa_idol_al);
set @permiso = (select count(aa_idol_est)  from ast  where aa_idol_est = 3 and aa_idol_al = pa_aa_idol_al);
set @ausente = (select count(aa_idol_est) from ast where aa_idol_est = 2 and aa_idol_al = pa_aa_idol_al);

set @total_ast = (select count(aa_idol_ast) from ast where aa_idol_al = pa_aa_idol_al);

if @permiso > 0 then 

set @porcentaje_permiso = (@permiso * 100) / (@total_ast);

else if @permiso <= 0 then

set @porcentaje_permiso = 0; 

else if @ausente > 0 then 

set @porcentaje_ausente = (@ausente * 100) / (@total_ast);

else if @ausente <= 0 then

set @porcentaje_ausente = 0; 

end if;
end if;
end if;
end if;


set @porcentaje_presente = (@presente * 100) / (@total_ast); 

Insert into asta(aa_idol_asta, aa_idol_al, aa_porp_asta, aa_porper_asta, aa_porf_asta)
values(0, pa_aa_idol_al,  @presente, @permiso, @ausente);
END//
DELIMITER ;

-- PA de la tabla asistencias por grado

DELIMITER //
CREATE PROCEDURE pa_stdc(pa_aa_idol_grad int)
BEGIN 

set @dummy = 1;

set @total_stdc = (select count(aa_idol_ast) from ast where aa_idol_grad = pa_aa_idol_grad);

-- variables de asistencia presente general

set @enerop = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @febrerop = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @marzop = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @abrilp = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @mayop = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @ajuniop = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @juliop = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @agostop = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @septiembrep = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @octubrep = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @noviembrep = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad);

if @enerop > 0 then 

set @enerop = (@enerop * 100) / (@total_stdc);

else if @enerop <= 0 then

set @enerop = 0; 

else if @febrerop > 0 then 

set @febrerop = (@febrerop * 100) / (@total_stdc);

else if @febrerop <= 0 then

set @febrerop = 0; 

else if @marzop > 0 then 

set @marzop = (@marzop * 100) / (@total_stdc);

else if @marzop <= 0 then

set @marzop = 0; 

else if @abrilp > 0 then 

set @abrilp = (@abrilp * 100) / (@total_stdc);

else if @abrilp <= 0 then

set @abrilp = 0;

else if @mayop > 0 then 

set @mayop = (@mayop * 100) / (@total_stdc);

else if @mayop <= 0 then

set @mayop = 0; 

else if @juniop > 0 then 

set @juniop = (@juniop * 100) / (@total_stdc);

else if @juniop <= 0 then

set @juniop = 0;

else if @juliop > 0 then 

set @juliop = (@juliop * 100) / (@total_stdc);

else if @juliop <= 0 then

set @juliop = 0; 

else if @agostop > 0 then 

set @agostop = (@agostop * 100) / (@total_stdc);

else if @agostop <= 0 then

set @agostop = 0;

else if @septiembrep > 0 then 

set @septiembrep = (@septiembrep * 100) / (@total_stdc);

else if @septiembrep <= 0 then

set @septiembrep = 0; 

else if @octubrep > 0 then 

set @octubrep = (@octubrep * 100) / (@total_stdc);

else if @octubrep <= 0 then

set @octubrep = 0;

else if @noviembrep > 0 then 

set @noviembrep = (@noviembrep * 100) / (@total_stdc);

else if @noviembrep <= 0 then

set @noviembrep = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- asistencia presente por genero femenino 
set @eneropf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @febreropf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @marzopf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @abrilpf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @mayopf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @ajuniopf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @juliopf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @agostopf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @septiembrepf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @octubrepf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @noviembrepf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);

if @eneropf > 0 then 

set @eneropf = (@eneropf * 100) / (@total_stdc);

else if @eneropf <= 0 then

set @eneropf = 0; 

else if @febreropf > 0 then 

set @febreropf = (@febreropf * 100) / (@total_stdc);

else if @febreropf <= 0 then

set @febreropf = 0; 

else if @marzopf > 0 then 

set @marzopf = (@marzopf * 100) / (@total_stdc);

else if @marzopf <= 0 then

set @marzopf = 0; 

else if @abrilpf > 0 then 

set @abrilpf = (@abrilpf * 100) / (@total_stdc);

else if @abrilpf <= 0 then

set @abrilpf = 0;

else if @mayopf > 0 then 

set @mayopf = (@mayopf * 100) / (@total_stdc);

else if @mayopf <= 0 then

set @mayopf = 0; 

else if @juniopf > 0 then 

set @juniopf = (@juniopf * 100) / (@total_stdc);

else if @juniopf <= 0 then

set @juniopf = 0;

else if @juliopf > 0 then 

set @juliopf = (@juliopf * 100) / (@total_stdc);

else if @juliopf <= 0 then

set @juliopf = 0; 

else if @agostopf > 0 then 

set @agostopf = (@agostopf * 100) / (@total_stdc);

else if @agostopf <= 0 then

set @agostopf = 0;

else if @septiembrepf > 0 then 

set @septiembrepf = (@septiembrepf * 100) / (@total_stdc);

else if @septiembrepf <= 0 then

set @septiembrepf = 0; 

else if @octubrepf > 0 then 

set @octubrepf = (@octubrepf * 100) / (@total_stdc);

else if @octubrepf <= 0 then

set @octubrepf = 0;

else if @noviembrepf > 0 then 

set @noviembrepf = (@noviembrepf * 100) / (@total_stdc);

else if @noviembrepf <= 0 then

set @noviembrepf = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- asistencia presente por genero masculino 

set @eneropm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @febreropm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @marzopm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @abrilpm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @mayopm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @ajuniopm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @juliopm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @agostopm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @septiembrepm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @octubrepm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @noviembrepm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 1 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);

if @eneropm > 0 then 

set @eneropm = (@eneropm * 100) / (@total_stdc);

else if @eneropmm <= 0 then

set @eneropm = 0; 

else if @febreropm > 0 then 

set @febreropm = (@febreropm * 100) / (@total_stdc);

else if @febreropm <= 0 then

set @febreropm = 0; 

else if @marzopm > 0 then 

set @marzopm = (@marzopm * 100) / (@total_stdc);

else if @marzopm <= 0 then

set @marzopm = 0; 

else if @abrilpm > 0 then 

set @abrilpm = (@abrilp * 100) / (@total_stdc);

else if @abrilpm <= 0 then

set @abrilpm = 0;

else if @mayopm > 0 then 

set @mayopm = (@mayop * 100) / (@total_stdc);

else if @mayopm <= 0 then

set @mayopm = 0; 

else if @juniopm > 0 then 

set @juniopm = (@juniopm * 100) / (@total_stdc);

else if @juniopm <= 0 then

set @juniopm = 0;

else if @juliopm > 0 then 

set @juliopm = (@juliopm * 100) / (@total_stdc);

else if @juliopm <= 0 then

set @juliopm = 0; 

else if @agostopm > 0 then 

set @agostopm = (@agostopm * 100) / (@total_stdc);

else if @agostopm <= 0 then

set @agostopm = 0;

else if @septiembrepm > 0 then 

set @septiembrepm = (@septiembrepm * 100) / (@total_stdc);

else if @septiembrepm <= 0 then

set @septiembrepm = 0; 

else if @octubrepm > 0 then 

set @octubrepm = (@octubrepm * 100) / (@total_stdc);

else if @octubrepm <= 0 then

set @octubrepm = 0;

else if @noviembrepm > 0 then 

set @noviembrepm = (@noviembrepm * 100) / (@total_stdc);

else if @noviembrepm <= 0 then

set @noviembrepm = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- asistencia estado ausente, general

set @eneroau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @febreroau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @marzoau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @abrilau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @mayoau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @ajunioau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @julioau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @agostoau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @septiembreau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @octubreau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @noviembreau = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad);

if @eneroau > 0 then 

set @eneroau = (@eneroau * 100) / (@total_stdc);

else if @eneroau <= 0 then

set @eneroau = 0; 

else if @febreroau > 0 then 

set @febreroau = (@febreroau * 100) / (@total_stdc);

else if @febreroau <= 0 then

set @febreroau = 0; 

else if @marzoau > 0 then 

set @marzoau = (@marzoau * 100) / (@total_stdc);

else if @marzoau <= 0 then

set @marzoau = 0; 

else if @abrilau > 0 then 

set @abrilau = (@abrilau * 100) / (@total_stdc);

else if @abrilau <= 0 then

set @abrilau = 0;

else if @mayoau > 0 then 

set @mayoau = (@mayoau * 100) / (@total_stdc);

else if @mayoau <= 0 then

set @mayoau = 0; 

else if @junioau > 0 then 

set @junioau = (@junioau * 100) / (@total_stdc);

else if @junioau <= 0 then

set @junioau = 0;

else if @julioau > 0 then 

set @julioau = (@julioau * 100) / (@total_stdc);

else if @julioau <= 0 then

set @julioau = 0; 

else if @agostoau > 0 then 

set @agostoau = (@agostoau * 100) / (@total_stdc);

else if @agostoau <= 0 then

set @agostoau = 0;

else if @septiembreau > 0 then 

set @septiembreau = (@septiembreau * 100) / (@total_stdc);

else if @septiembreau <= 0 then

set @septiembreau = 0; 

else if @octubreau > 0 then 

set @octubreau = (@octubreau * 100) / (@total_stdc);

else if @octubreau <= 0 then

set @octubreau = 0;

else if @noviembreau > 0 then 

set @noviembreau = (@noviembreau * 100) / (@total_stdc);

else if @noviembreau <= 0 then

set @noviembreau = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- ausencias por genero femenino 

set @eneroauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @febreroauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @marzoauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @abrilauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @mayoauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @ajunioauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @julioauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @agostoauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @septiembreauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @octubreauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @noviembreauf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);

if @eneroauf > 0 then 

set @eneroauf = (@eneroauf * 100) / (@total_stdc);

else if @eneroauf <= 0 then

set @eneroauf = 0; 

else if @febreroauf > 0 then 

set @febreroauf = (@febreroauf * 100) / (@total_stdc);

else if @febreroauf <= 0 then

set @febreroauf = 0; 

else if @marzoauf > 0 then 

set @marzoauf = (@marzoauf * 100) / (@total_stdc);

else if @marzoauf <= 0 then

set @marzoauf = 0; 

else if @abrilauf > 0 then 

set @abrilauf = (@abrilauf * 100) / (@total_stdc);

else if @abrilauf <= 0 then

set @abrilauf = 0;

else if @mayoauf > 0 then 

set @mayoauf = (@mayoauf * 100) / (@total_stdc);

else if @mayoauf <= 0 then

set @mayoauf = 0; 

else if @junioauf > 0 then 

set @junioauf = (@junioauf * 100) / (@total_stdc);

else if @junioauf <= 0 then

set @junioauf = 0;

else if @julioauf > 0 then 

set @julioauf = (@julioauf * 100) / (@total_stdc);

else if @julioauf <= 0 then

set @julioauf = 0; 

else if @agostoauf > 0 then 

set @agostoauf = (@agostoauf * 100) / (@total_stdc);

else if @agostoauf <= 0 then

set @agostoauf = 0;

else if @septiembreauf > 0 then 

set @septiembreauf = (@septiembreauf * 100) / (@total_stdc);

else if @septiembreauf <= 0 then

set @septiembreauf = 0; 

else if @octubreauf > 0 then 

set @octubreauf = (@octubreauf * 100) / (@total_stdc);

else if @octubreauf <= 0 then

set @octubreauf = 0;

else if @noviembreauf > 0 then 

set @noviembreauf = (@noviembreauf * 100) / (@total_stdc);

else if @noviembreauf <= 0 then

set @noviembreauf = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;


-- ausencias por genero masculino 

set @eneroaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @febreroaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @marzoaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @abrilaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @mayoaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @ajunioaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @julioaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @agostoaum  = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @septiembreaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @octubreaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @noviembreaum = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 2 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);

if @eneroaum > 0 then 

set @eneroaum = (@eneroaum * 100) / (@total_stdc);

else if @eneroaum <= 0 then

set @eneroaum = 0; 

else if @febreroaum > 0 then 

set @febreroaum = (@febreroaum * 100) / (@total_stdc);

else if @febreroaum <= 0 then

set @febreroaum = 0; 

else if @marzoaum > 0 then 

set @marzoaum = (@marzoaum * 100) / (@total_stdc);

else if @marzoaum <= 0 then

set @marzoaum = 0; 

else if @abrilaum > 0 then 

set @abrilaum = (@abrilaum * 100) / (@total_stdc);

else if @abrilaum <= 0 then

set @abrilaum = 0;

else if @mayoaum > 0 then 

set @mayoaum = (@mayoaum * 100) / (@total_stdc);

else if @mayoaum <= 0 then

set @mayoaum = 0; 

else if @junioaum > 0 then 

set @junioaum = (@junioaum * 100) / (@total_stdc);

else if @junioaum <= 0 then

set @junioaum = 0;

else if @julioaum > 0 then 

set @julioaum = (@julioaum * 100) / (@total_stdc);

else if @julioaum <= 0 then

set @julioaum = 0; 

else if @agostoaum > 0 then 

set @agostoaum = (@agostoaum * 100) / (@total_stdc);

else if @agostoaum <= 0 then

set @agostoaum = 0;

else if @septiembreaum > 0 then 

set @septiembreaum = (@septiembreaum * 100) / (@total_stdc);

else if @septiembreaum <= 0 then

set @septiembreaum = 0; 

else if @octubreaum > 0 then 

set @octubreaum = (@octubreaum * 100) / (@total_stdc);

else if @octubreaum <= 0 then

set @octubreaum = 0;

else if @noviembreaum > 0 then 

set @noviembreaum = (@noviembreaum * 100) / (@total_stdc);

else if @noviembreaum <= 0 then

set @noviembreaum = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;



-- asistencias con permiso a nivel de grado

set @eneroper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @febreroper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @marzoper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @abrilper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @mayoper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @ajunioper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @julioper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @agostoper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @septiembreper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @octubreper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @noviembreper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad);

if @eneroper > 0 then 

set @eneroper = (@eneroper * 100) / (@total_stdc);

else if @eneroper <= 0 then

set @eneroper = 0; 

else if @febreroper > 0 then 

set @febreroper = (@febrerop * 100) / (@total_stdc);

else if @febreroper <= 0 then

set @febreroper = 0; 

else if @marzoper > 0 then 

set @marzoper = (@marzoper * 100) / (@total_stdc);

else if @marzoper <= 0 then

set @marzoper = 0; 

else if @abrilper > 0 then 

set @abrilper = (@abrilper * 100) / (@total_stdc);

else if @abrilper <= 0 then

set @abrilper = 0;

else if @mayoper > 0 then 

set @mayoper = (@mayoper * 100) / (@total_stdc);

else if @mayoper <= 0 then

set @mayoper = 0; 

else if @junioper > 0 then 

set @junioper = (@junioper * 100) / (@total_stdc);

else if @junioper <= 0 then

set @junioper = 0;

else if @julioper > 0 then 

set @julioper = (@julioper * 100) / (@total_stdc);

else if @julioper <= 0 then

set @julioper = 0; 

else if @agostoper > 0 then 

set @agostoper = (@agostoper * 100) / (@total_stdc);

else if @agostoper <= 0 then

set @agostoper = 0;

else if @septiembreper > 0 then 

set @septiembreper = (@septiembreper * 100) / (@total_stdc);

else if @septiembreper <= 0 then

set @septiembreper = 0; 

else if @octubreper > 0 then 

set @octubreper = (@octubreper * 100) / (@total_stdc);

else if @octubreper <= 0 then

set @octubreper = 0;

else if @noviembreper > 0 then 

set @noviembreper = (@noviembreper * 100) / (@total_stdc);

else if @noviembreper <= 0 then

set @noviembreper = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- permiso por femenino

set @eneroperf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @febreroperf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @marzoperf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @abrilperf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @mayoperf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @ajunioperf = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @julioper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @agostoper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @septiembreper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @octubreper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);
set @noviembreper = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 2);

if @eneroperf > 0 then 

set @eneroperf = (@eneroperf * 100) / (@total_stdc);

else if @eneroperf <= 0 then

set @eneroperf = 0; 

else if @febreroperf > 0 then 

set @febreroperf = (@febreroperf * 100) / (@total_stdc);

else if @febreroperf <= 0 then

set @febreroperf = 0; 

else if @marzoperf > 0 then 

set @marzoperf = (@marzoperf * 100) / (@total_stdc);

else if @marzoperf <= 0 then

set @marzoperf = 0; 

else if @abrilperf > 0 then 

set @abrilperf = (@abrilperf * 100) / (@total_stdc);

else if @abrilperf <= 0 then

set @abrilperf = 0;

else if @mayoperf > 0 then 

set @mayoperf = (@mayoperf * 100) / (@total_stdc);

else if @mayoperf <= 0 then

set @mayoperf = 0; 

else if @junioperf > 0 then 

set @junioperf = (@junioperf * 100) / (@total_stdc);

else if @junioperf <= 0 then

set @junioperf = 0;

else if @julioperf > 0 then 

set @julioperf = (@julioperf * 100) / (@total_stdc);

else if @julioperf <= 0 then

set @julioperf = 0; 

else if @agostoperf > 0 then 

set @agostoperf = (@agostoperf * 100) / (@total_stdc);

else if @agostoperf <= 0 then

set @agostoperf = 0;

else if @septiembreperf > 0 then 

set @septiembreperf = (@septiembreperf * 100) / (@total_stdc);

else if @septiembreperf <= 0 then

set @septiembreperf = 0; 

else if @octubreperf > 0 then 

set @octubreperf = (@octubreperf * 100) / (@total_stdc);

else if @octubreperf <= 0 then

set @octubreperf = 0;

else if @noviembreperf > 0 then 

set @noviembreperf = (@noviembreperf * 100) / (@total_stdc);

else if @noviembreperf <= 0 then

set @noviembreperf = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- permiso masculino 

set @eneroperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @febreroperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @marzoperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @abrilperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @mayoperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @ajunioperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @julioperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @agostoperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @septiembreperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @octubreperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);
set @noviembreperm = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 3 AND ga.aa_idol_grad = pa_aa_idol_grad  AND g.aa_idol_gnr = 1);

if @eneroperm > 0 then 

set @eneroperm = (@eneroperm * 100) / (@total_stdc);

else if @eneroperm <= 0 then

set @eneroperm = 0; 

else if @febreroperm > 0 then 

set @febreroperm = (@febreropm * 100) / (@total_stdc);

else if @febreroperm <= 0 then

set @febreroperm = 0; 

else if @marzoperm > 0 then 

set @marzoperm = (@marzoperm * 100) / (@total_stdc);

else if @marzoperm <= 0 then

set @marzoperm = 0; 

else if @abrilperm > 0 then 

set @abrilperm = (@abrilperm * 100) / (@total_stdc);

else if @abrilperm <= 0 then

set @abrilperm = 0;

else if @mayoperm > 0 then 

set @mayoperm = (@mayoperm * 100) / (@total_stdc);

else if @mayoperm <= 0 then

set @mayoperm = 0; 

else if @junioperm > 0 then 

set @junioperm = (@junioperm * 100) / (@total_stdc);

else if @junioperm <= 0 then

set @junioperm = 0;

else if @julioperm > 0 then 

set @julioperm = (@julioperm * 100) / (@total_stdc);

else if @julioperm <= 0 then

set @julioperm = 0; 

else if @agostoperm > 0 then 

set @agostoperm = (@agostoperm * 100) / (@total_stdc);

else if @agostoperm <= 0 then

set @agostoperm = 0;

else if @septiembreperm > 0 then 

set @septiembreperm = (@septiembreperm * 100) / (@total_stdc);

else if @septiembreperm <= 0 then

set @septiembreperm = 0; 

else if @octubreperm > 0 then 

set @octubreperm = (@octubreperm * 100) / (@total_stdc);

else if @octubreperm <= 0 then

set @octubreperm = 0;

else if @noviembreperm > 0 then 

set @noviembreperm = (@noviembreperm * 100) / (@total_stdc);

else if @noviembreperm <= 0 then

set @noviembreperm = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- Cuarentena 

set @enerocua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @febrerocua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @marzocua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est =  8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @abrilcua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @mayocua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @ajuniocua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @juliocua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @agostocua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @septiembrecua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @octubrecua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @noviembrecua = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 8 AND ga.aa_idol_grad = pa_aa_idol_grad);

if @enerocua > 0 then 

set @enerocua = (@enerocua * 100) / (@total_stdc);

else if @enerocua <= 0 then

set @enerocua = 0; 

else if @febrerocua > 0 then 

set @febrerocua = (@febrerocua * 100) / (@total_stdc);

else if @febrerocua <= 0 then

set @febrerocua = 0; 

else if @marzocua > 0 then 

set @marzocua = (@marzocua * 100) / (@total_stdc);

else if @marzocua <= 0 then

set @marzocua = 0; 

else if @abrilcua > 0 then 

set @abrilcua = (@abrilcua * 100) / (@total_stdc);

else if @abrilcua <= 0 then

set @abrilcua = 0;

else if @mayocua > 0 then 

set @mayocua = (@mayocua * 100) / (@total_stdc);

else if @mayocua <= 0 then

set @mayocua = 0; 

else if @juniocua > 0 then 

set @juniocua = (@juniocua * 100) / (@total_stdc);

else if @juniocua <= 0 then

set @juniocua = 0;

else if @juliocua > 0 then 

set @juliocua = (@juliocua * 100) / (@total_stdc);

else if @juliocua <= 0 then

set @juliocua = 0; 

else if @agostocua > 0 then 

set @agostocua = (@agostocua * 100) / (@total_stdc);

else if @agostocua <= 0 then

set @agostocua = 0;

else if @septiembrecua > 0 then 

set @septiembrecua = (@septiembrecua * 100) / (@total_stdc);

else if @septiembrecua <= 0 then

set @septiembrecua = 0; 

else if @octubrepcua > 0 then 

set @octubrecua = (@octubrecua * 100) / (@total_stdc);

else if @octubrecua <= 0 then

set @octubrecua = 0;

else if @noviembrecua > 0 then 

set @noviembrecua = (@noviembrecua * 100) / (@total_stdc);

else if @noviembrecua <= 0 then

set @noviembrecua = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- Ememrgencia Nacional

set @eneroe = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @febreroe = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @marzoe = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @abrile = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @mayoe = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @ajunioe = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @julioe = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @agostoe = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @septiembree = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @octubree = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @noviembree = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 9 AND ga.aa_idol_grad = pa_aa_idol_grad);

if @eneroe > 0 then 

set @eneroe = (@eneroe * 100) / (@total_stdc);

else if @eneroe <= 0 then

set @eneroe = 0; 

else if @febreroe > 0 then 

set @febreroe = (@febreroe * 100) / (@total_stdc);

else if @febreroe <= 0 then

set @febreroe = 0; 

else if @marzoe > 0 then 

set @marzoe = (@marzoe * 100) / (@total_stdc);

else if @marzoe <= 0 then

set @marzoe = 0; 

else if @abrile > 0 then 

set @abrile = (@abrile * 100) / (@total_stdc);

else if @abrile <= 0 then

set @abrile = 0;

else if @mayoe > 0 then 

set @mayoe = (@mayoe * 100) / (@total_stdc);

else if @mayoe <= 0 then

set @mayoe = 0; 

else if @junioe > 0 then 

set @junioe = (@junioe * 100) / (@total_stdc);

else if @junioe <= 0 then

set @junioe = 0;

else if @julioe > 0 then 

set @julioe = (@julioe * 100) / (@total_stdc);

else if @julioe <= 0 then

set @julioe = 0; 

else if @agostoe > 0 then 

set @agostoe = (@agostoe * 100) / (@total_stdc);

else if @agostoe <= 0 then

set @agostoe = 0;

else if @septiembree > 0 then 

set @septiembree = (@septiembree * 100) / (@total_stdc);

else if @septiembree <= 0 then

set @septiembree = 0; 

else if @octubree > 0 then 

set @octubree = (@octubree * 100) / (@total_stdc);

else if @octubree <= 0 then

set @octubree = 0;

else if @noviembree > 0 then 

set @noviembree = (@noviembree * 100) / (@total_stdc);

else if @noviembree <= 0 then

set @noviembree = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;

-- catastrofe

set @enerocat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 1 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @febrerocat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 2 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @marzocat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 3 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad );
set @abrilcat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 4 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @mayocat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 5 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @ajuniocat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 6 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @juliocat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) =  7 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @agostocat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 8 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @septiembrecat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 9 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @octubrecat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 10 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);
set @noviembrecat = (SELECT count(*) from ast ae INNER JOIN al a ON ae.aa_idol_al = a.aa_idol_al INNER JOIN gnr g ON a.aa_idol_gnr = g.aa_idol_gnr INNER JOIN grad ga ON a.aa_idol_grad = ga.aa_idol_grad WHERE MONTH(aa_fecha_ast) = 11 AND YEAR(aa_fecha_ast) = 2020 AND aa_idol_est = 10 AND ga.aa_idol_grad = pa_aa_idol_grad);

if @enerocat > 0 then 

set @enerocat = (@enerocat * 100) / (@total_stdc);

else if @enerocat <= 0 then

set @enerocat = 0; 

else if @febrerocat > 0 then 

set @febrerocat = (@febrerocat * 100) / (@total_stdc);

else if @febrerocat <= 0 then

set @febrerocat = 0; 

else if @marzocat > 0 then 

set @marzocat = (@marzocat * 100) / (@total_stdc);

else if @marzocat <= 0 then

set @marzocat= 0; 

else if @abrilcat > 0 then 

set @abrilcat = (@abrilcat * 100) / (@total_stdc);

else if @abrilcat <= 0 then

set @abrilcat = 0;

else if @mayocat > 0 then 

set @mayocat = (@mayocat * 100) / (@total_stdc);

else if @mayocat <= 0 then

set @mayocat = 0; 

else if @juniocat > 0 then 

set @juniocat = (@juniocat * 100) / (@total_stdc);

else if @juniocat <= 0 then

set @juniocat = 0;

else if @juliocat > 0 then 

set @juliocat = (@juliocat * 100) / (@total_stdc);

else if @juliocat <= 0 then

set @juliocat = 0; 

else if @agostocat > 0 then 

set @agostocat = (@agostocat * 100) / (@total_stdc);

else if @agostocat <= 0 then

set @agostocat = 0;

else if @septiembrecat > 0 then 

set @septiembrecat = (@septiembrecat * 100) / (@total_stdc);

else if @septiembrecat <= 0 then

set @septiembrecat = 0; 

else if @octubrecat > 0 then 

set @octubrecat = (@octubrecat * 100) / (@total_stdc);

else if @octubrecat <= 0 then

set @octubrecat = 0;

else if @noviembrecat > 0 then 

set @noviembrecat = (@noviembrecat * 100) / (@total_stdc);

else if @noviembrecat <= 0 then

set @noviembrecat = 0;

end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;
end if;







END//
DELIMITER ;



CALL pa_asta(1);
