<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<script type="text/javascript" src="src/js/jquery-3.4.1.js"></script> 
<script type="text/javascript" src="src/js/bootstrap.js"></script>
<script src="https://kit.fontawesome.com/a076d05399.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<script src="https://unpkg.com/imask"></script>
<link rel="stylesheet" type="text/css" href="src/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="src/css/menuh.css"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
<title>Ingreso de asistencia de alumno</title>
</head>
<body style="background-color: #060606;">
<!-- seccion de menu -->
        <div class="container">
            <div class="row">
                <div class="col-8" style="margin-left:-200px">
                    <div class="nav-side-menu">
                        <div class="brand">AsysCon - Toma de asistencia</div>
                        <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

                        <div class="menu-list">
                            <ul id="menu-content" class="menu-content collapse out">
                                <li>
                                    <a href="PaginaPrincipal.jsp">
                                        <i class="fas fa-school"></i> Volver al Men&uacute; Principal
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
           </div>
                           
           
           
           <!-- fin de menu -->      
           
           <div class="row">
                <div class="col-4"></div>
                <div class="col-8">
                 <br> <br> <br> <br> <br>
                    <p id = "titulo"></p>
                       </div>
           </div>
<div class="row">
	<div class="col-4"></div><div class="col-8">
	<br><br>
	<table class="table-bordered">
		<thead class="table-success">
			<tr>
				<td>cod Alumno</td>
				<td>Nombre</td>
				<td>Apellido</td>
				<td>Tomar asistencia</td>				
			</tr>
		</thead>
		<tbody class="table-light">
			<c:forEach items="${lista}" var="e">
				<tr>
					<td>${e.aaCodAl}</td>
					<td>${e.aaNomAl}</td>
					<td>${e.aaApeAl}</td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i data-original="fas fa-apple-alt" class="fas fa-apple-alt" id="dia1"></i></button></td>
				</tr>
			</c:forEach>
			</tbody>
	</table>
	 </div>  
	                
				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Asignar Asistencia</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form method="POST" action="guardaAsistencia">
				         <div class="form-group">
                                        <label for="message-text" class="col-form-label">Alumno</label>
                                        <input type="hidden" name="aaIdolAl" id="aaIdolAl" />
                                        <input type="text" class="form-control" name="aaNomAl" id="aaNomAl" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="col-form-label">fecha</label>
                                        <input type="date" class="form-control" name="aaFechaAst" id="aaFechaAst">
                                    </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Estado:</label>
                                            <select name="aaIdolEst" id="aaIdolEst">
                                                <c:forEach items="${listaestado}" var="s">
                                                    <option value="${s.aaIdolEst}">${s.aaNomEst}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-dark" data-dismiss="modal">cerrar</button>
                                            <button class="btn btn-outline-success">Guardar</button>
                                        </div>
                                </form>
				    </div>
				  </div>
				</div>
				      </div>             
				</div>                   
         </div>   
           
         
         <script type="text/javascript">

         var mititulo = document.getElementById("titulo").innerHTML = "<h1 style='color: #20B620; text-align:center;'>Toma de asistencia</h1>";
         	
                        
                        $(document).ready(function(){
                              $('select').formSelect();
                          });
                        
                        function obteniendoAlumno(aaIdolAl, aaNomAl) {
                            /* Tomando los valores desde el javascript */
                            document.getElementById("aaIdolAl").value = aaIdolAl;
                            document.getElementById("aaNomAl").value = aaNomAl;
                        }

                        function  insertandoAsistencia(aaIdolAl, aaFechaAst, aaIdolEst) {
                            /* Tomando los valores desde el javascript */
                            document.getElementById("aaIdolAl").value = aaIdolAl;
                            document.getElementById("aaFechaAst").value = aaFechaAst;
                            document.getElementById("aaIdolEst").value = aaIdolEst; 
                    
                        }

                        /*function validafecha(aaIdolAl, aaFechaAst, aaIdolEst){
                        	/* Tomando los valores desde el javascript */
                            /*document.getElementById("aaIdolAl").value = aaIdolAl;
                            document.getElementById("aaFechaAst").value = aaFechaAst;
                            document.getElementById("aaIdolEst").value = aaIdolEst; 

                          var hoy = new Date();
                        	var anio = hoy.getFullYear().toString();
                        	var fechaFormulario = new Date(aaFechaAst);

                        	// Comparamos solo las fechas => no las horas!!
                        	hoy.setHours(0,0,0,0);  // Lo iniciamos a 00:00 horas

                        	var dateMask = IMask(
                        			document.getElementById("aaFechaAst"),
                        			  {
                        			    mask: Date,
                        			    min: new Date(1990, 0, 1),
                        			    max: new Date(2021, 0, 1),
                        			    lazy: false
                        			  });

                        	if ((hoy < fechaFormulario) || (hoy > fechaFormulario)) {
                        		document.getElementById("aaFechaAst").style.boxShadow = '0 0 15px red';
                            	document.getElementById("aaFechaAst").value = "";
                        	alertify.notify('Debe de ingresar la fecha actual', 'error',10, null);  
                        	return false;
                        	}
                        	else {

                        		document.getElementById("aaFechaAst").style.boxShadow = '0 0 15px green';
                               
                            	
                        	}//cierre else

                        	

                        	return true;
                                
                            
                        }

                        */
                        function guardado_exitoso(){
                        	Swal.fire({
                				title: '¡Asistencia Agregada!',
                				text: "Asistencia insertada con exito.",
                				icon: 'success',
                				showCancelButton: false,
                				confirmButtonColor: '#3085d6',
                				confirmButtonText: 'Aceptar'
                			}).then((result) => {
                				if (result.value) {
                					window.location.href = "${pageContext.request.contextPath}/vistaAsistencia"; 
                				}
                			})
                           
                        
                    	}

                       
                    </script>    
                    
</body>
</html>