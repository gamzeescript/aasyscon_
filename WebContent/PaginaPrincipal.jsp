<%-- 
    Document   : pruebaPrincipal
    Created on : Mar 1, 2020, 6:57:00 PM
    Author     : Michelle Giron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <title>Bienvenido AsysCon</title>
        <meta name="description" content="Circle Hover Effects with CSS Transitions" />
        <meta name="keywords" content="circle, border-radius, hover, css3, transition, image, thumbnail, effect, 3d" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="src/css/demo.css" />
		<link rel="stylesheet" type="text/css" href="src/css/common.css" />
        <link rel="stylesheet" type="text/css" href="src/css/style7.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700' rel='stylesheet' type='text/css' />
		<script type="text/javascript" src="src/js/modernizr.custom.79639.js"></script> 
		<!--[if lte IE 8]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
    </head>
    <body>
        <div class="container">
        	
			<h1></h1>
			<header>
                                 <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>
				
				
				<div class="support-note"><!-- let's check browser support with modernizr -->
					<!--span class="no-cssanimations">CSS animations are not supported in your browser</span-->
					<span class="no-csstransforms">CSS transforms are not supported in your browser</span>
					<span class="no-csstransforms3d">CSS 3D transforms are not supported in your browser</span>
					<span class="no-csstransitions">CSS transitions are not supported in your browser</span>
					<span class="note-ie">Sorry, only modern browsers.</span>
				</div>
				
			</header>
			
			<section class="main">			
				<ul class="ch-grid">			
					<li>					
						<div class="ch-item">				
							<div class="ch-info">
								<div class="ch-info-front ch-img-1"></div>
								<div class="ch-info-back">
									<h3>Nomina</h3>
									<a href="${pageContext.request.contextPath}/nominaMatricula">
									<p>Alumnos matriculados</p>
									</a>
								</div>	
							</div>
						</div>
					</li>
					<li>				
						<div class="ch-item">
							<div class="ch-info">
								<div class="ch-info-front ch-img-2"></div>
								<div class="ch-info-back">
									<h3>Asistencia</h3>
									<a href="${pageContext.request.contextPath}/vistaAsistencia">									 
									<p>Toma de asistencia y procesamiento de informacioacute;n</p>
									</a>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="ch-item">
							<div class="ch-info">
								<div class="ch-info-front ch-img-3"></div>
								<div class="ch-info-back">
									<h3>Estadisticas</h3>
									<a href="${pageContext.request.contextPath}/recorrerMes">	
									<p>Porcentajes, reportes y graficos de asistencia</p>
									</a>
								</div>
							</div>
						</div>
                                            
					</li>
                                        <li>
                                        <div class="ch-item">
							<div class="ch-info">
								<div class="ch-info-front ch-img-4"></div>
								<div class="ch-info-back">
									<h3>Historial</h3>
									<a href="${pageContext.request.contextPath}/statitics">
									<p>Listado de alumnos y asistencias por mes</p>
									</a>
								</div>
							</div>
						</div>
                                        </li>
				</ul>
				
			</section>
        </div>
    </body>
</html>


