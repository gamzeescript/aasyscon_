<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ingreso de asistencia de alumno - Enero</title>
<link rel="stylesheet" type="text/css" href="../src/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../src/css/menuh.css"/>
<script type="text/javascript" src="../../src/js/bootstrap.js"></script> 
<script type="text/javascript" src="../../src/js/jquery-3.4.1.js"></script> 
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>
<body>
 <!--include de mi header  -->
        <jsp:include page="../menuh.jsp"/> 
        
        <div class="row">
                <div class="col-4"></div>
                <div class="col-8">
                 <br> <br> <br> <br> <br>
                    <p id = "titulo"></p>
            <div class="row">
                <div class="col-12">     
        <br><br>
	<table class="table-bordered">
		<thead class="table-success">
			<tr>
				<td>cod Alumno</td>
				<td>Nombre</td>
				<td>Apellido</td>
				<td>1</td>
				<td>2</td>
				<td>3</td>
				<td>4</td>
				<td>5</td>
				<td>6</td>
				<td>7</td>
				<td>8</td>
				<td>9</td>
				<td>10</td>
				<td>11</td>
				<td>12</td>
				<td>13</td>
				<td>14</td>
				<td>15</td>
				<td>16</td>
				<td>17</td>
				<td>18</td>
				<td>19</td>
				<td>20</td>
				<td>21</td>
				<td>22</td>
				<td>23</td>
				<td>24</td>
				<td>25</td>
				<td>26</td>
				<td>27</td>
				<td>28</td>
				<td>29</td>
				<td>30</td>
				<td>31</td>
			</tr>
		</thead>
		<tbody class="table-light">
			<c:forEach items="${lista}" var="e">
				<tr>
					<td>${e.aaCodAl}</td>
					<td>${e.aaNomAl}</td>
					<td>${e.aaApeAl}</td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i data-original="fas fa-apple-alt" class="fas fa-apple-alt" id="dia1"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia2"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia3"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia4"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia5"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia6"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia7"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia8"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia9"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia10"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia11"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia12"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia13"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia14"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia15"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia16"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia17"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia18"></i></button></td>
		            <td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia19"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia20"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia21"></i></button></td>
                    <td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia22"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia23"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia24"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia25"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia26"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia27"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia28"></i></button></td>
				    <td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia29"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia30"></i></button></td>
					<td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModal" onclick="obteniendoAlumno('${e.aaIdolAl}','${e.aaNomAl}')"><i class="fas fa-apple-alt" id="dia31"></i></button></td>

				</tr>
			</c:forEach>
			</tbody>
	</table>
	                
				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Asignar Asistencia</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form method="POST" action="guardaAsistencia">
				         <div class="form-group">
                                        <label for="message-text" class="col-form-label">Alumno</label>
                                        <input type="hidden" name="aaIdolAl" id="aaIdolAl" />
                                        <input type="text" class="form-control" name="aaNomAl" id="aaNomAl" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="col-form-label">fecha</label>
                                        <input type="date" class="form-control" name="aaFechaAst" id="aaFechaAst">
                                    </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Estado:</label>
                                            <select name="aaIdolEst" id="aaIdolEst">
                                                <c:forEach items="${listaestado}" var="s">
                                                    <option value="${s.aaIdolEst}">${s.aaNomEst}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-dark" data-dismiss="modal">cerrar</button>
                                            <button class="btn btn-outline-success"  onclick="reload(true)">Guardar</button>
                                        </div>
                                </form>
				    </div>
				  </div>
				</div>
				      </div>     
        
         </div>
				  </div>
				</div>
				      </div>     
        
         
         <script type="text/javascript">
                        
                        $(document).ready(function(){
                              $('select').formSelect();
                          });
                        
                        function obteniendoAlumno(aaIdolAl, aaNomAl) {
                            /* Tomando los valores desde el javascript */
                            document.getElementById("aaIdolAl").value = aaIdolAl;
                            document.getElementById("aaNomAl").value = aaNomAl;
                        }

                        function  insertandoAsistencia(aaIdolAl, aaFechaAst, aaIdolEst, dia1, dia2, dia3, dia4, dia5, dia6, dia7, dia8, dia9, dia10, dia11, dia12, dia13, dia14, dia15, dia16, dia17, dia18, dia19, dia20, dia21, dia22, dia23, dia24, dia25, dia26, dia27, dia28, dia29, dia30, dia31) {
                            /* Tomando los valores desde el javascript */
                            document.getElementById("aaIdolAl").value = aaIdolAl;
                            document.getElementById("aaFechaAst").value = aaFechaAst;
                            document.getElementById("aaIdolEst").value = aaIdolEst;  
                            
                            
                        }

                        


                    </script>    
</body>
</html>