<%-- 
    Document   : pruebaLogin
    Created on : Mar 1, 2020, 11:30:45 AM
    Author     : Michelle Giron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Asyscon</title>
	<link rel="shortcut icon" href="src/img/checking.ico"/>
  <meta charset="UTF-8">
  <title>AsysCon</title>
  <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="src/css/style.css">
  <link rel="stylesheet" href="src/css/Toast.css">
  <script type="text/javascript" src="src/js/Toast-ES6.js"></script>
</head>
<body>
 
<div class="container">
  <div class="left-section">
    <div class="header">
      <img src="src/img/cheking.png" alt="80" width="100" class="animation a1"/><br>
      <h1 class="animation a1">AsysCon</h1>
      <h4 class="animation a2">Iniciar sesion</h4>
    </div>
    <form action="login" method="POST">
    <div class="form">
      <input type="text" maxlength="6"  class="form-field animation a3 inputcolor" placeholder="Usuario" name="usr" required>
      <input type="password" maxlength="7" class="form-field animation a4 inputcolor" placeholder="Password" name="prw" required>
      <button class="animation a6" id="inicio">Iniciar Sesion</button>
      <label id="labeldeaviso">${msg}</label>
    </div>
    </form>
  </div>
  <div class="right-section"></div>
</div>
  
</body>
</html>
