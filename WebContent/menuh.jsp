<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Asistencia de meses</title>
<script type="text/javascript" src="src/js/jquery-3.4.1.js"></script> 
<script type="text/javascript" src="src/js/bootstrap.js"></script> 
<link rel="stylesheet" type="text/css" href="src/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="src/css/menuh.css"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>
<body style="background-color: #060606;">
<!-- seccion de menu -->
        <div class="container">
            <div class="row">
                <div class="col-8" style="margin-left:-200px">
                    <div class="nav-side-menu">
                        <div class="brand">AsysCon - Toma de asistencia</div>
                        <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

                        <div class="menu-list">
                            <ul id="menu-content" class="menu-content collapse out">
                                <li>
                                    <a href="PaginaPrincipal.jsp">
                                        <i class="fas fa-school"></i> Volver al Men&uacute; Principal
                                    </a>
                                </li>
                                <li>	<a href="${pageContext.request.contextPath}/enero">
                                        <i class="fa fa-users fa-lg" id="enero"></i> Enero
                                        </a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/febrero"> 
                                        <i class="fa fa-users fa-lg" id="febrero"></i> Febrero
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/marzo">
                                        <i class="fa fa-users fa-lg" id="marzo"></i> Marzo
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/abril">
                                        <i class="fa fa-users fa-lg" id="abril"></i> Abril
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/mayo">
                                        <i class="fa fa-users fa-lg" id="mayo"></i> Mayo
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/junio">
                                        <i class="fa fa-users fa-lg" id="junio"></i> Junio
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/julio">
                                        <i class="fa fa-users fa-lg" id="julio"></i> Julio
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/agosto">
                                        <i class="fa fa-users fa-lg" id="agosto"></i> Agosto
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/septiembre">
                                        <i class="fa fa-users fa-lg" id="septiembre"></i> Septiembre
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/octubre">
                                        <i class="fa fa-users fa-lg" id="octubre"></i> Octubre
                                    </a>
                                </li>
                                 <li>
                                    <a href="${pageContext.request.contextPath}/noviembre">
                                        <i class="fa fa-users fa-lg" id="noviembre"></i> Noviembre
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
           </div>
                           
           
           
           <!-- fin de menu -->      
           
           <div class="row">
                <div class="col-4"></div>
                <div class="col-8">
                 <br> <br> <br> <br> <br>
                    <p id = "titulo"></p>
                       </div>
           </div>
           
                   
                      
        </div>    
<script type="text/javascript">
var a = new Date();
var mititulo = document.getElementById("titulo").innerHTML = "<h1 style='color: #20B620; text-align:center;'>A&ntilde;o lectivo "+a.getFullYear()+"</h1>";
	

</script>