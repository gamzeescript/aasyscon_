<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<script type="text/javascript" src="src/js/bootstrap.js"></script> 
<script type="text/javascript" src="src/js/jquery-3.4.1.js"></script> 
<link rel="stylesheet" type="text/css" href="src/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="src/css/menuh.css"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<title>Nomina Alumnos</title>
<style>

body {

background-color: #060606;

}

a.enlace {

color: #a4d792;
}

div.posicion {

padding-right: 60%;

}

</style>

</head>
<body>
	<table class="table-bordered">
		<thead class="table-success">
			<tr>
				<td>Id</td>
				<td>cod Alumno</td>
				<td>Nombre</td>
				<td>Apellido</td>
				<td>Fecha Nacimiento</td>
				<td>grado</td>
				<td>seccion</td>
				<td>tipo de estudio</td>
				<td>turno</td>
				<td>direccion</td>
				<td>telefono</td>
				<td>Responsable</td>
				<td>parentesco</td>
				<td>telefono responsable</td>
			</tr>
		</thead>
		<tbody class="table table-light">
			<c:forEach items="${lista}" var="e">
				<tr>
					<td>${e.aaIdolAl}</td>
					<td>${e.aaCodAl}</td>
					<td>${e.aaNomAl}</td>
					<td>${e.aaApeAl}</td>
					<td>${e.aaFnacAl}</td>
					<td>${e.grad.aaNomGrad}</td>
					<td>${e.scn.aaNomScn}</td>
					<td>${e.tpd.aaNomTpd}</td>
					<td>${e.trn.aaNomTrn}</td>
					<td>${e.aaDirAl}</td>
					<td>${e.aaTlfAl}</td>
					<td>${e.aaPrAl}</td>
					<td>${e.aaPrtAl}</td>
					<td>${e.aaPtlfAl}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<br><br><br><br><br>
	<div class="col-4"></div><div class="col-8">
	<div class="enlace">
	<a href="PaginaPrincipal.jsp" class="enlace">Volver</a>
	</div>
	</div>
</body>
</html>