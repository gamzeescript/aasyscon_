package com.ampersand.pri.control;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ampersand.pri.imp.AlumnoRep;
import com.ampersand.pri.imp.AsistenciaRep;
import com.ampersand.pri.imp.EstadoRep;
import com.ampersand.pri.imp.MesRep;
import com.ampersand.pri.model.Al;
import com.ampersand.pri.model.Ast;
import com.ampersand.pri.model.Est;

@Controller
public class TomaAsistencia {

	@Autowired
	@Qualifier("astRep")
	private AsistenciaRep astRep;
	
	@Autowired
	@Qualifier("alRep")
	private AlumnoRep alRep;
	
	@Autowired
	@Qualifier("msRep")
	private MesRep msRep;
	
	
	@Autowired
	@Qualifier("estRep")
	private EstadoRep estRep;
	
	private ModelAndView mv = new ModelAndView();
	
	
	/*Metodo que despliega la vista para tomar asistencia*/
	
	
	@RequestMapping(value = "/vistaAsistencia", method = RequestMethod.GET)
	public ModelAndView asignacionAsistencia() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("AsistenciaAlumno");
		return mv;
		
	}
	
	
	/* metodo que guarda la asistencia
	 * pide distintos parametros que se procesan de diversos repositorios
	 * como el de alumno y el de asistencia. Los parametros son id del alumno
	 * fecha y el id del estado */
	
	@RequestMapping(value = "/guardaAsistencia",  method = RequestMethod.POST)
	public ModelAndView checkeado(@RequestParam("aaIdolAl") String aaIdolAl, @RequestParam("aaFechaAst") String aaFechaAst, @RequestParam("aaIdolEst") String aaIdolEst) {
		ModelAndView mv = new ModelAndView("AsistenciaAlumno");
		
		Ast asistencia = new Ast();
		Al alumno = new Al();
		Est estado = new Est();
		estado.setAaIdolEst(Integer.parseInt(aaIdolEst));
		alumno.setAaIdolAl(Integer.parseInt(aaIdolAl));
		asistencia.setAl(alumno);	         	
		asistencia.setAaFechaAst(aaFechaAst);
		asistencia.setEst(estado);
		astRep.create(asistencia);
		
		System.out.println(asistencia.getAl());
		System.out.println(asistencia.getAaFechaAst());
		System.out.println(asistencia.getEst());
		
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		return mv;
		
	}
	
	
	/* Cambiar icono */
	
	
	
	
	
	

}
