package com.ampersand.pri.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Estadisticas {
	
	private ModelAndView mv = new ModelAndView();
	
	
	/*metodo para poder ver las estadisticas*/
	@RequestMapping(value = "/statitics", method = RequestMethod.GET)
	public ModelAndView historialAsistencia() {
		mv.setViewName("Estadisticas");
		return mv;
	}
}
