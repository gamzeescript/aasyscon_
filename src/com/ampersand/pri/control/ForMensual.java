package com.ampersand.pri.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ampersand.pri.imp.AlumnoRep;
import com.ampersand.pri.imp.AsistenciaRep;
import com.ampersand.pri.imp.EstadoRep;
import com.ampersand.pri.imp.MesRep;
import com.ampersand.pri.model.Al;
import com.ampersand.pri.model.Ast;
import com.ampersand.pri.model.Est;
import com.ampersand.pri.model.Ms;

@Controller
public class ForMensual {

	@Autowired
	@Qualifier("astRep")
	private AsistenciaRep astRep;
	
	@Autowired
	@Qualifier("alRep")
	private AlumnoRep alRep;
	
	@Autowired
	@Qualifier("msRep")
	private MesRep msRep;
	
	@Autowired
	@Qualifier("estRep")
	private EstadoRep estRep;
	
	private ModelAndView mv = new ModelAndView();
	
	
	/* metodos para recorrer meses*/
	
	@RequestMapping(value = "/recorrerMes", method = RequestMethod.GET)
	public ModelAndView esteMes() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("AsistenciaporMes");
		return mv;
	}
	
	@RequestMapping(value = "/enero", method = RequestMethod.GET)
	public ModelAndView mesEnero() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/enero");
		return mv;
	}
	
	
	
	@RequestMapping(value = "/febrero", method = RequestMethod.GET)
	public ModelAndView mesFebrero() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/febrero");
		return mv;
	}
	
	@RequestMapping(value = "/marzo", method = RequestMethod.GET)
	public ModelAndView mesMarzo() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/marzo");
		return mv;
	}
	
	@RequestMapping(value = "/abril", method = RequestMethod.GET)
	public ModelAndView mesAbril() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/abril");
		return mv;
	}
	
	@RequestMapping(value = "/mayo", method = RequestMethod.GET)
	public ModelAndView mesMayo() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/mayo");
		return mv;
	}
	
	@RequestMapping(value = "/junio", method = RequestMethod.GET)
	public ModelAndView mesJunio() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/junio");
		return mv;
	}
	
	@RequestMapping(value = "/julio", method = RequestMethod.GET)
	public ModelAndView mesJulio() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/julio");
		return mv;
	}
	
	@RequestMapping(value = "/agosto", method = RequestMethod.GET)
	public ModelAndView mesAgosto() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/agosto");
		return mv;
	}
	
	@RequestMapping(value = "/septiembre", method = RequestMethod.GET)
	public ModelAndView mesSeptiembre() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/septiembre");
		return mv;
	}
	
	@RequestMapping(value = "/octubre", method = RequestMethod.GET)
	public ModelAndView mesOctubre() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/octubre");
		return mv;
	}
	
	@RequestMapping(value = "/noviembre", method = RequestMethod.GET)
	public ModelAndView mesNoviembre() {
		mv.addObject("lista", alRep.read());
		mv.addObject("listaestado", estRep.read());
		mv.setViewName("meses/noviembre");
		return mv;
	}
	

	
	/** 
	@ResponseBody
	@RequestMapping(value = "/chooseMonth", method = RequestMethod.GET)
	public ModelAndView escogeMes(@RequestParam("enero") String enero, @RequestParam("febrero") String febrero, @RequestParam("marzo") String marzo, @RequestParam("abril") String abril, @RequestParam("mayo") String mayo, @RequestParam("junio") String junio, @RequestParam("julio") String julio, @RequestParam("agosto") String agosto, @RequestParam("septiembre") String septiembre, @RequestParam("octubre") String octubre, @RequestParam("noviembre") String noviembre) {		
		 if (msRep.escogeMes(enero)==null) {
			 mv.setViewName("meses/enero");
				return mv;
		 }
		 if (msRep.escogeMes(febrero)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
			 
		 }
		 if (msRep.escogeMes(marzo)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
		 }
		 if (msRep.escogeMes(abril)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
		 }
		 if (msRep.escogeMes(mayo)==null) {
			 mv.setViewName("meses/febrero");
				return mv; 
		 }
		 if (msRep.escogeMes(junio)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
		 }
		 if (msRep.escogeMes(julio)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
		 }
		 if (msRep.escogeMes(agosto)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
		 }
		 if (msRep.escogeMes(septiembre)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
		 }
		 if (msRep.escogeMes(octubre)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
		 }
		 if (msRep.escogeMes(noviembre)==null) {
			 mv.setViewName("meses/febrero");
				return mv;
		 }		
		
		 mv.setViewName("AsistenciaporMes");
			return mv;
	}
	
	**/
	
}
