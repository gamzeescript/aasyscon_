package com.ampersand.pri.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ampersand.pri.imp.AlumnoRep;
import com.ampersand.pri.imp.EstadoRep;
import com.ampersand.pri.imp.MesRep;

@Controller
public class Matriculado {
	
	
	@Autowired
	@Qualifier("alRep")
	private AlumnoRep alRep;
	
	@Autowired
	@Qualifier("estRep")
	private EstadoRep estRep;
	
	private ModelAndView mv = new ModelAndView();
	
	/* Mi redirecicion a mi lista de alumnos */
	
	@RequestMapping(value = "/nominaMatricula", method = RequestMethod.GET)
	public ModelAndView listadomatricula() {
		mv.addObject("lista", alRep.read());
		System.out.println("aqui estoyyy");
		mv.setViewName("NominaAlumno");
		return mv;
		
	}
	
	

	
	

	
	
}
