package com.ampersand.pri.control;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ampersand.pri.imp.UsuarioRep;
import com.ampersand.pri.model.Usr;

@Controller
public class InicioSesion {
	
	@Autowired
	@Qualifier("usrRep")
	private UsuarioRep usrRep;
	
	private ModelAndView mv = new ModelAndView();
	
	/*metodo de redireccion de login
	 * pide dos parametros para dar acceso al sistema */
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(@RequestParam("usr")String usr, @RequestParam("prw")String prw, HttpServletRequest answer) {
		HttpSession session = answer.getSession();
		try {
			System.out.println("aqui estoyyy ");
			List<Usr> listaUsr = usrRep.Login(usr, prw);
			System.out.println("aqui estoyyy hola de nuevo");
			if(listaUsr.isEmpty()) {
				System.out.println("anos destruira a todos!!");
				mv.setViewName("Login");
				return mv;
			}
			else {
				session.setAttribute("us", listaUsr.get(0).getAaIdolUsr());
				session.setAttribute("nom", listaUsr.get(0).getAaUsrUsr());
				session.setAttribute("rl", listaUsr.get(0).getRl());
				return new ModelAndView("PaginaPrincipal");
			}
		}catch(Exception e) {
			System.out.println("Error: "+usr+" "+prw);
			System.out.println("el error es: "+e.getMessage());
			e.printStackTrace();
			
		}
		return mv;
	}
	
	/*Metodo para cerrar sesion */
	
	@RequestMapping(value = "/logoff", method = RequestMethod.GET)
	public ModelAndView logoff(HttpServletRequest request) {
		HttpSession sessionoff = request.getSession(false);
		if(sessionoff != null) {
			sessionoff.invalidate();
			return new ModelAndView("LOGIN");
		}
		return null;
		
	}

}
