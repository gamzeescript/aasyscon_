package com.ampersand.pri.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HistoricoAsistencia {
	
	private ModelAndView mv = new ModelAndView();
	
	/*metodo para consultar historico*/
	
	@RequestMapping(value = "/historial", method = RequestMethod.GET)
	public ModelAndView historialAsistencia() {
		mv.setViewName("Historico");
		return mv;
	}

}
