package com.ampersand.pri.imp;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ampersand.pri.model.Al;
import com.ampersand.pri.util.AFacade;
import com.ampersand.pri.util.DAO;

@Transactional
public class AlumnoRep extends AFacade<Al> implements DAO<Al>{
	
	CriteriaBuilder cb;
	CriteriaQuery<Al> cq;

	 @Autowired
	    private SessionFactory sessionFactory;

	    public AlumnoRep() {
	        super(Al.class);
	    }

	    @Override
	    public SessionFactory sessionFactory() {
	        return sessionFactory;
	    }
	    
	    /* Metodo lista de alumnos por id, los buscamos por id 
	     * y lo seleccionamos para que pueda procesarse junto con la 
	     * asistencia*/
	    
	    public Al listaAlumnoporId(int idol) {
	    	Al alumnos = new Al();
	    	List<Al> listaAlumno = new LinkedList<>();
	    	try {
	    		session.beginTransaction();
	    		cb  = session.getCriteriaBuilder();
	    		cq = cb.createQuery(Al.class);
	    		Root<Al> aln = cq.from(Al.class);
	    		cq.where(cb.equal(aln.get("aaIdolAl"), idol));
	    		
	    		listaAlumno = session.createQuery(cq).getResultList();
	    		alumnos = listaAlumno.get(0);
	    		session.getTransaction().commit();	    		
	    		return alumnos;
	    	}
	    	catch(Exception e){
	    		e.printStackTrace();
	    		return null;
	    	}
	    	
	    	
	    }
	    
	
}
