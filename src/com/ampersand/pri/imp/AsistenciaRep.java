package com.ampersand.pri.imp;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ampersand.pri.model.Ast;
import com.ampersand.pri.model.Est;
import com.ampersand.pri.util.AFacade;
import com.ampersand.pri.util.DAO;

@Transactional
public class AsistenciaRep extends AFacade<Ast> implements DAO<Ast>{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public AsistenciaRep() {
		super(Ast.class);
	}
	
	@Override
	public SessionFactory sessionFactory() {
		return sessionFactory;
	}

	public Est obtenerEstado() {
		
		
		return null;
	}
	
	/* buscar por estado - utilizado para un dropdown 
	 * que permite escoger las opciones para guardar la 
	 * asistencia, se utilizo queries nativas
	 *  */
	
	
	
	public List<Ast> seleccionEstado(int aaIdolEst){
		List<Ast> listaAst = new LinkedList<>();
		try {
			session.beginTransaction();
			listaAst = session.createNativeQuery("select * from ast where aa_idol_est = '" +aaIdolEst+ "'", Ast.class).getResultList();
			session.flush();
			session.getTransaction().commit();
			return listaAst;
		}catch(Exception e) {
			session.clear();
			return null;
		}
		
	}
	
	
}
