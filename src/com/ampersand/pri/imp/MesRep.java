package com.ampersand.pri.imp;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ampersand.pri.model.Ms;
import com.ampersand.pri.util.AFacade;
import com.ampersand.pri.util.DAO;

@Transactional
public class MesRep extends AFacade<Ms> implements DAO<Ms>{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public MesRep() {
		super(Ms.class);
	}
	
	public SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	/*Metodo que escoge un mes de la tabla de meses para facilitar el acceso a las vistas logicas asignadas
	 * se utilizo una query de tipo HQL- inutilizado */
	
	public List<Ms> escogeMes(String emes){
		List<Ms> lista = new LinkedList<>();
		try {
			String sql = "select m.* from ms m where m.aa_nom_ms = "+emes+";";
			Query q = sessionFactory().getCurrentSession().createNativeQuery(sql);
			if (!q.getResultList().isEmpty()) {
				lista = q.getResultList();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}


}