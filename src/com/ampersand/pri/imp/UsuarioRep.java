package com.ampersand.pri.imp;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ampersand.pri.model.Usr;
import com.ampersand.pri.util.AFacade;
import com.ampersand.pri.util.DAO;

public class UsuarioRep extends AFacade<Usr> implements DAO<Usr>{
	
	@Autowired
	private SessionFactory sessionFactory;

	public UsuarioRep() {
		super(Usr.class);
		
	}
	
	@Override
	public SessionFactory sessionFactory() {
		return sessionFactory;
	}

	/* Metodo de login que permite accesar al sistema se le pasa dos variables de tipo string
	 * utilizamos lo que es HQL que buscara un usuario y un password que coincidan con el registro
	 * usamos un objeto de tipo linkedList para procesar la lista que nos devolvera un resultado */
	
	public List<Usr> Login(String usr, String prw) {
		List<Usr> listaUsr = new LinkedList<>();
		try {
			String sql = "SELECT u FROM Usr u where u.aaUsrUsr = ?1 AND u.aaPrwUser = ?2";
			Query q = sessionFactory().getCurrentSession().createQuery(sql);
			q.setParameter(1, usr);
			q.setParameter(2, prw);
			listaUsr = q.getResultList();
			return listaUsr;
		}catch(Exception e) {
			System.out.println("Error en el login: " +e.getMessage());
			return null;
		}
		
		
			
		}
		
	
	
	
	

}
