package com.ampersand.pri.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ampersand.pri.model.Est;
import com.ampersand.pri.util.AFacade;
import com.ampersand.pri.util.DAO;

@Transactional
public class EstadoRep extends AFacade<Est> implements DAO<Est>{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EstadoRep() {
		super(Est.class);
	}
	
	@Override
	public SessionFactory sessionFactory() {
		return sessionFactory;
	}

}
