package com.ampersand.pri.model;
// Generated 02-24-2020 09:36:37 AM by Hibernate Tools 5.2.12.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Ast generated by hbm2java
 */
@Entity
@Table(name = "ast", catalog = "aasys")
public class Ast implements java.io.Serializable {

	private int aaIdolAst;
	private Al al;
	private Est est;
	private String aaFechaAst;
	private Set<Aspa> aspas = new HashSet<Aspa>(0);
	private Set<Stdc> stdcs = new HashSet<Stdc>(0);

	public Ast() {
	}

	public Ast(int aaIdolAst, Al al, Est est, String aaFechaAst) {
		this.aaIdolAst = aaIdolAst;
		this.al = al;
		this.est = est;
		this.aaFechaAst = aaFechaAst;
	}

	public Ast(int aaIdolAst, Al al, Est est, String aaFechaAst, Set<Aspa> aspas, Set<Stdc> stdcs) {
		this.aaIdolAst = aaIdolAst;
		this.al = al;
		this.est = est;
		this.aaFechaAst = aaFechaAst;
		this.aspas = aspas;
		this.stdcs = stdcs;
	}

	@Id

	@Column(name = "aa_idol_ast", unique = true, nullable = false)
	public int getAaIdolAst() {
		return this.aaIdolAst;
	}

	public void setAaIdolAst(int aaIdolAst) {
		this.aaIdolAst = aaIdolAst;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "aa_idol_al", nullable = false)
	public Al getAl() {
		return this.al;
	}

	public void setAl(Al al) {
		this.al = al;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "aa_idol_est", nullable = false)
	public Est getEst() {
		return this.est;
	}

	public void setEst(Est est) {
		this.est = est;
	}

	@Column(name = "aa_fecha_ast", nullable = false, length = 10)
	public String getAaFechaAst() {
		return this.aaFechaAst;
	}

	public void setAaFechaAst(String aaFechaAst) {
		this.aaFechaAst = aaFechaAst;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "ast")
	public Set<Aspa> getAspas() {
		return this.aspas;
	}

	public void setAspas(Set<Aspa> aspas) {
		this.aspas = aspas;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "ast")
	public Set<Stdc> getStdcs() {
		return this.stdcs;
	}

	public void setStdcs(Set<Stdc> stdcs) {
		this.stdcs = stdcs;
	}

}
