package com.ampersand.pri.util;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.ampersand.pri.imp.AlumnoRep;
import com.ampersand.pri.imp.AsistenciaRep;
import com.ampersand.pri.imp.EstadoRep;
import com.ampersand.pri.imp.MesRep;
import com.ampersand.pri.imp.UsuarioRep;

@Configuration
@EnableWebMvc
@EnableTransactionManagement(proxyTargetClass = true)
@ComponentScan(basePackages = {"com.ampersand"})
public class Configuracion implements WebMvcConfigurer{
	
	
	@Bean
	public MesRep msRep() {
		return new MesRep();
	}
	
	
	@Bean
	public AlumnoRep alRep() {
		return new AlumnoRep();
	}
	
	
	@Bean 
	public UsuarioRep usrRep() {
		return new UsuarioRep();
	}
	
	
	@Bean 
	public EstadoRep estRep() {
		return new EstadoRep();
	}
	
	
	@Bean
	public AsistenciaRep astRep() {
		return new AsistenciaRep();
	}
	
	@Autowired
	@Bean
	public InternalResourceViewResolver views() {
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setPrefix("/");
		vr.setSuffix(".jsp");
		return vr;
	}
	
	@Bean
	public DataSource dataSource() {
		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/aaSys?useSSL=false");
		ds.setUsername("root");
		ds.setPassword("root");		
		return ds;
		
	}
	
	private Properties getHibernateProperties() {
		Properties hibernateProperties = new Properties();
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return hibernateProperties;
		
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean f = new LocalSessionFactoryBean();
		f.setDataSource(dataSource());
		f.setHibernateProperties(getHibernateProperties());
		f.setPackagesToScan("com.ampersand.pri.model");
		return f;
	}
	
	@Bean
	public PlatformTransactionManager transactionManger() {
		HibernateTransactionManager hib = new HibernateTransactionManager();
		hib.setSessionFactory(sessionFactory().getObject());
		return hib;
	}
	
	 @Override
	    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	        registry.addResourceHandler("/src/**")
	                .addResourceLocations("/src/");
	    }  

   }