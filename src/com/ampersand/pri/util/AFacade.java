package com.ampersand.pri.util;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract class AFacade<T> {
	
	private Class<T> entityClass;

	public AFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public abstract SessionFactory sessionFactory();

	protected Session session;

	@Transactional
	public void create(T entity) {
		session = sessionFactory().getCurrentSession();
		session.save(entity);
	}

	@Transactional
	public void update(T entity) {
		session = sessionFactory().getCurrentSession();
		session.update(entity);
	}

	@Transactional
	public void delete(T entity) {
		session = sessionFactory().getCurrentSession();
		session.delete(entity);
	}

	@Transactional
	public T readById(Object id) {
		session = sessionFactory().getCurrentSession();
		return session.find(entityClass, id);
	}

	@Transactional
	public List<T> read() {
		session = sessionFactory().getCurrentSession();
		CriteriaQuery cq = session.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return session.createQuery(cq).getResultList();
	}


}
