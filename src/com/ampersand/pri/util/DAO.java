package com.ampersand.pri.util;

import java.util.List;

public interface DAO<T> {
	
	public void create(T e);

	public List<T> read();

	public T readById(Object id);

	public void update(T e);

	public void delete(T e);

}
