package com.ampersand.pri.util;

import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ConfiguracionSeguridad extends WebSecurityConfigurerAdapter {
	
		
		
		@Bean
		public BCryptPasswordEncoder passwordEncoder() {
			 return new BCryptPasswordEncoder();
			
		}
		
		@Bean
		public DaoAuthenticationProvider authenticationProvider() {
			   DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
			   authProvider.setUserDetailsService(userDetailsService());
			   authProvider.setPasswordEncoder(passwordEncoder());
			   return authProvider;
			
		}
		
		

}
